import resolve from 'rollup-plugin-node-resolve'
import commonjs from 'rollup-plugin-commonjs'
import svelte from 'rollup-plugin-svelte'

export default {
  entry: 'src/index.js',
  dest: 'dist/index.js',
  format: 'umd',
  plugins: [
    resolve({
      extensions: ['.js', '.html'],
      module: true,
      jsnext: true,
      main: true,
      browser: true,
      preferBuiltins: true,
      customResolveOptions: {
        moduleDirectory: ['node_modules', 'src']
      }
    }),
    commonjs(),
    svelte()
  ]
}
