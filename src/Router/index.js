// Router
import StateRouter from 'abstract-state-router'
import SvelteRenderer from 'svelte-state-renderer'

const stateRouter = StateRouter(
  SvelteRenderer(),
  document.querySelector('#App')
)
stateRouter.setMaxListeners(20)

export { stateRouter }
