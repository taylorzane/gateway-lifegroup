import Develop_TrustGod_Study from './Develop/TrustGod/Study'

const topics = {
  // Discover
  TrustChrist: {
    title: 'Trust Christ',
    group: 'Discover',
    sections: {
      Study: ``,
      Read: ``,
      Apply: ``,
      Memorize: ``
    }
  },
  // Develop
  BuildOthers: {
    title: 'Build Others',
    group: 'Develop',
    sections: {
      Study: ``,
      Read: ``,
      Apply: ``
    }
  },
  Confession: {
    title: 'Confession',
    group: 'Develop',
    sections: {
      Study: ``,
      Read: ``,
      Apply: ``
    }
  },
  FinancialGenerosity: {
    title: 'Financial Generosity',
    group: 'Develop',
    sections: {
      Study: ``,
      Read: ``,
      Apply: ``
    }
  },
  Freedom: {
    title: 'Freedom',
    group: 'Develop',
    sections: {
      Study: ``,
      Read: ``,
      Apply: ``
    }
  },
  Humility: {
    title: 'Humility',
    group: 'Develop',
    sections: {
      Study: ``,
      Read: ``,
      Apply: ``
    }
  },
  Intercession: {
    title: 'Intercession',
    group: 'Develop',
    sections: {
      Study: ``,
      Read: ``,
      Apply: ``
    }
  },
  NewCreation: {
    title: 'New Creation',
    group: 'Develop',
    sections: {
      Study: ``,
      Read: ``,
      Apply: ``
    }
  },
  ObeyGod: {
    title: 'Obey God',
    group: 'Develop',
    sections: {
      Study: ``,
      Read: ``,
      Apply: ``
    }
  },
  Outreach: {
    title: 'Outreach',
    group: 'Develop',
    sections: {
      Study: ``,
      Read: ``,
      Apply: ``
    }
  },
  Peacemaker: {
    title: 'Peacemaker',
    group: 'Develop',
    sections: {
      Study: ``,
      Read: ``,
      Apply: ``
    }
  },
  RelateToGod: {
    title: 'Relate to God',
    group: 'Develop',
    sections: {
      Study: ``,
      Read: ``,
      Apply: ``
    }
  },
  RelationallyEngaged: {
    title: 'Relationally Engaged',
    group: 'Develop',
    sections: {
      Study: ``,
      Read: ``,
      Apply: ``
    }
  },
  SelfAwareness: {
    title: 'Self-Awareness',
    group: 'Develop',
    sections: {
      Study: ``,
      Read: ``,
      Apply: ``
    }
  },
  Serve: {
    title: 'Serve',
    group: 'Develop',
    sections: {
      Study: ``,
      Read: ``,
      Apply: ``
    }
  },
  SpiritualDisciplines: {
    title: 'Spiritual Disciplines',
    group: 'Develop',
    sections: {
      Study: ``,
      Read: ``,
      Apply: ``
    }
  },
  SpiritualGifts: {
    title: 'Spiritual Gifts',
    group: 'Develop',
    sections: {
      Study: ``,
      Read: ``,
      Apply: ``
    }
  },
  StayConnectedToGodsSpirit: {
    title: "Stay Connected to God's Spirit",
    group: 'Develop',
    sections: {
      Study: ``,
      Read: ``,
      Apply: ``
    }
  },
  StudyGod: {
    title: 'Study God',
    group: 'Develop',
    sections: {
      Study: ``,
      Read: ``,
      Apply: ``
    }
  },
  TrustGod: {
    title: 'Trust God',
    group: 'Develop',
    sections: {
      Study: Develop_TrustGod_Study,
      Read: ``,
      Apply: ``
    }
  },
  WorshipGod: {
    title: 'Worship God',
    group: 'Develop',
    sections: {
      Study: ``,
      Read: ``,
      Apply: ``
    }
  },
  // Deepen
  Surrender: {
    title: 'Surrender',
    group: 'Deepen',
    sections: {
      Study: ``,
      Read: ``,
      Apply: ``,
      Memorize: ``
    }
  },
  Wisdom: {
    title: 'Wisdom',
    group: 'Deepen',
    sections: {
      Study: ``,
      Read: ``,
      Apply: ``,
      Memorize: ``
    }
  },
  SelfFeeding: {
    title: 'Self-Feeding',
    group: 'Deepen',
    sections: {
      Study: ``,
      Read: ``,
      Apply: ``,
      Memorize: ``
    }
  },
  SteadinessAndPerseverance: {
    title: 'Steadiness and Perseverance',
    group: 'Deepen',
    sections: {
      Study: ``,
      Read: ``,
      Apply: ``,
      Memorize: ``
    }
  },
  Sacrifice: {
    title: 'Sacrifice',
    group: 'Deepen',
    sections: {
      Study: ``,
      Read: ``,
      Apply: ``,
      Memorize: ``
    }
  },
  Faithfulness: {
    title: 'Faithfulness',
    group: 'Deepen',
    sections: {
      Study: ``,
      Read: ``,
      Apply: ``,
      Memorize: ``
    }
  },
  Ministry: {
    title: 'Ministry',
    group: 'Deepen',
    sections: {
      Study: ``,
      Read: ``,
      Apply: ``,
      Memorize: ``
    }
  },
  Multiplication: {
    title: 'Multiplication',
    group: 'Deepen',
    sections: {
      Study: ``,
      Read: ``,
      Apply: ``,
      Memorize: ``
    }
  }
}

export { topics }
