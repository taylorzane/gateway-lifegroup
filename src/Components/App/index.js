import template from './index.html'

import { TopicList, Topic, SubTopic } from 'Components'

export const App = stateRouter => {
  stateRouter.addState({
    name: 'App',
    route: '/app',
    defaultChild: 'TopicList',
    template
  })

  TopicList(stateRouter)
  Topic(stateRouter)
  SubTopic(stateRouter)
}
