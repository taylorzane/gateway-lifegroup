import template from './index.html'

export const TopicList = stateRouter => {
  stateRouter.addState({
    name: 'App.TopicList',
    route: '/topics',
    template
  })
}
