import template from './index.html'

export const SubTopic = stateRouter => {
  stateRouter.addState({
    name: 'App.SubTopic',
    route: '/topic/:topic/:sub_topic',
    template,
    activate({ parameters, domApi: svelte }) {
      svelte.set({
        topic: parameters.topic
      })

      svelte.set({
        sub_topic: parameters.sub_topic
      })
    }
  })
}
