import template from './index.html'

export const Topic = stateRouter => {
  stateRouter.addState({
    name: 'App.Topic',
    route: '/topic/:topic',
    template,
    activate({ parameters, domApi: svelte }) {
      svelte.set({ topic: parameters.topic })
    }
  })
}
